﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
	[Serializable]
	class Intervenant
	{
		private string _nom;
		private decimal _tauxHoraire;
		
		public decimal getTauxHoraire()
		{
			return _tauxHoraire;
		}

		public Intervenant(string nom, decimal tauxHoraire)
		{
			_nom = nom;
			_tauxHoraire = tauxHoraire;
		}
	}
}
