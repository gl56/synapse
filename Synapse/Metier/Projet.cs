﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
	[Serializable]
	class Projet
	{
		private string _nom;
		private DateTime _début;
		private DateTime _fin;
		private int _prixFacturéMO;
		private List<Mission> _listDesMissions;

		private decimal CumulCoutMO()
		{
			decimal cout = 0;
			int nbHeure = 0;
			decimal taux = 0;
			foreach (Mission mission in _listDesMissions)
			{
				taux = mission.getExecutant().getTauxHoraire();
				nbHeure = nbHeure + mission.NbHeureEffectuees();
				cout = cout + (taux * nbHeure);
			}
			return cout;
		}

		public decimal margeBruteCourante()
		{
			decimal prixfact = _prixFacturéMO;
			decimal cumul = this.CumulCoutMO();
			prixfact = prixfact - cumul;
			return prixfact;
		}
		public void AjouterMission(string nomM, string description, int nbHeuresPrevues, Intervenant executant)
		{
			Mission mission = new Mission(nomM, description, nbHeuresPrevues, executant);
			_listDesMissions.Add(mission);
		}

		public List<Mission> ListeMissions
		{
			get { return _listDesMissions; }
			set { _listDesMissions = value; }
		}

		public string Nom
		{
			get { return _nom; }
			set { _nom = value; }
		}

		public DateTime Debut
		{
			get { return _début; }
			set { _début = value; }
		}

		public DateTime Fin
		{
			get { return _fin; }
			set { _fin = value; }
		}

		public int PrixFact
		{
			get { return _prixFacturéMO; }
			set { _prixFacturéMO = value; }
		}

		public Projet(string nom, DateTime début, DateTime fin, int prixFacturéMO)
		{
			_nom = nom;
			_début = début;
			_fin = fin;
			_prixFacturéMO = prixFacturéMO;
			_listDesMissions = new List<Mission>();
		}
	}
}
