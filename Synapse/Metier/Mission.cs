﻿using System;             
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metier
{
	[Serializable]
	class Mission
	{
		private string _nom;
		private string _description;
		private int _nbHeuresPrevues;
		private Intervenant _executant;
		private Dictionary<DateTime,int> _releveHoraire;

		public Intervenant getExecutant()
		{
			return _executant;
		}

		public Dictionary<DateTime,int> getReleveHoraire()
		{
			return _releveHoraire;
		}

		public int NbHeureEffectuees()
		{
			int _nbHeuresFaites = 0;
			foreach (KeyValuePair<DateTime,int> releve in _releveHoraire)
			{
				_nbHeuresFaites = _nbHeuresFaites + releve.Value;
			}
			return _nbHeuresFaites;
		}

		public void AjoutReleve(DateTime date, int nbHeures)
		{
			_releveHoraire.Add(date, nbHeures);
		}

		public Mission(string nom, string description, int nbHeuresPrevues, Intervenant executant)
		{
			_nom = nom;
			_description = description;
			_nbHeuresPrevues = nbHeuresPrevues;
			_executant = executant;
			_releveHoraire = new Dictionary<DateTime, int>();
		}
	}
}