﻿namespace Synapse.Vues
{
	partial class FormAjoutProjet
	{
		/// <summary>
		/// Variable nécessaire au concepteur.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Nettoyage des ressources utilisées.
		/// </summary>
		/// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Code généré par le Concepteur Windows Form

		/// <summary>
		/// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
		/// le contenu de cette méthode avec l'éditeur de code.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.buttonAjoutProjet = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.label4 = new System.Windows.Forms.Label();
			this.monthCalendarProjet = new System.Windows.Forms.MonthCalendar();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxCoutMO = new System.Windows.Forms.TextBox();
			this.textBoxNom = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.buttonAjoutProjet);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(7, 10);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(409, 350);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Projet";
			// 
			// buttonAjoutProjet
			// 
			this.buttonAjoutProjet.Location = new System.Drawing.Point(226, 285);
			this.buttonAjoutProjet.Name = "buttonAjoutProjet";
			this.buttonAjoutProjet.Size = new System.Drawing.Size(165, 58);
			this.buttonAjoutProjet.TabIndex = 1;
			this.buttonAjoutProjet.Text = "Ajouter le projet";
			this.buttonAjoutProjet.UseVisualStyleBackColor = true;
			this.buttonAjoutProjet.Click += new System.EventHandler(this.buttonAjoutProjet_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.listBox1);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.monthCalendarProjet);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.textBoxCoutMO);
			this.groupBox2.Controls.Add(this.textBoxNom);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox2.Location = new System.Drawing.Point(20, 22);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(371, 257);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			// 
			// listBox1
			// 
			this.listBox1.BackColor = System.Drawing.SystemColors.Menu;
			this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 15;
			this.listBox1.Items.AddRange(new object[] {
            "(Veuillez selectionner ",
            "une date de début et ",
            "une date de fin)"});
			this.listBox1.Location = new System.Drawing.Point(6, 133);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(126, 49);
			this.listBox1.TabIndex = 10;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(-3, 152);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(0, 15);
			this.label4.TabIndex = 9;
			// 
			// monthCalendarProjet
			// 
			this.monthCalendarProjet.Location = new System.Drawing.Point(133, 84);
			this.monthCalendarProjet.Name = "monthCalendarProjet";
			this.monthCalendarProjet.TabIndex = 8;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(14, 117);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(107, 17);
			this.label3.TabIndex = 6;
			this.label3.Text = "Durée du projet";
			// 
			// textBoxCoutMO
			// 
			this.textBoxCoutMO.Location = new System.Drawing.Point(257, 49);
			this.textBoxCoutMO.Name = "textBoxCoutMO";
			this.textBoxCoutMO.Size = new System.Drawing.Size(103, 23);
			this.textBoxCoutMO.TabIndex = 3;
			this.textBoxCoutMO.Leave += new System.EventHandler(this.textBoxCoutMO_Leave);
			// 
			// textBoxNom
			// 
			this.textBoxNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBoxNom.Location = new System.Drawing.Point(117, 22);
			this.textBoxNom.Name = "textBoxNom";
			this.textBoxNom.Size = new System.Drawing.Size(243, 21);
			this.textBoxNom.TabIndex = 2;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(14, 52);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(245, 17);
			this.label2.TabIndex = 1;
			this.label2.Text = "Coût approximatif de la main d\'oeuvre";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(14, 25);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(97, 17);
			this.label1.TabIndex = 0;
			this.label1.Text = "Nom du projet";
			// 
			// FormAjoutProjet
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(425, 362);
			this.Controls.Add(this.groupBox1);
			this.Name = "FormAjoutProjet";
			this.Text = "FormAjoutProjet";
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBoxCoutMO;
		private System.Windows.Forms.TextBox textBoxNom;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonAjoutProjet;
		private System.Windows.Forms.MonthCalendar monthCalendarProjet;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ListBox listBox1;
	}
}

