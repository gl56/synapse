﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Utilitaire;
using Synapse.Metier;
using Synapse.Properties;

namespace Synapse.Vues
{
	public partial class FormListeProjetsMissions : Form
	{
		public FormListeProjetsMissions()
		{
			InitializeComponent();
		}
		private Projet _projetselectionne;

		private void FormListeProjetsMissions_Load(object sender, EventArgs e)
		{
			dataGridViewProjet.DataSource = Donnees.CollectionProjets;
			dataGridViewMission.DataSource = _projetselectionne.ListeMissions;
		}

		private void dataGridViewProjet_RowEnter(object sender, DataGridViewCellEventArgs e)
		{
			if (dataGridViewProjet.SelectedRows.Count > 0)
			{
				foreach (Projet projet in Donnees.CollectionProjets)
				{
					if (projet.Equals(_projetselectionne))
					{
						_projetselectionne.Nom = projet.Nom;
						_projetselectionne.Debut = projet.Debut;
						_projetselectionne.Fin = projet.Fin;
						_projetselectionne.PrixFact = projet.PrixFact;
						_projetselectionne.ListeMissions = projet.ListeMissions;
					}
				}
			}
		}
	}
}
