﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Utilitaire;
using Synapse.Metier;
using Synapse.Properties;


namespace Synapse.Vues
{
	public partial class FormAjoutProjet : Form
	{
		private int cout;

		public FormAjoutProjet()
		{
			InitializeComponent();
		}

		private void textBoxCoutMO_Leave(object sender, EventArgs e)
		{
			if (textBoxCoutMO.Text != "")
			{
				cout = Convert.ToInt16(textBoxCoutMO.Text);
				if (cout == 0)
				{
					MessageBox.Show("Veuillez saisir le coût du projet en chiffre(entier)!");
					textBoxCoutMO.Clear();
					textBoxCoutMO.Focus();
				}
			}
		}

		private void buttonAjoutProjet_Click(object sender, EventArgs e)
		{
			if (textBoxNom.Text == "")
			{
				MessageBox.Show("Veuillez donner un nom au projet!");
			}
			else if (textBoxCoutMO.Text == "")
			{
				MessageBox.Show("Veuillez saisir le coût du projet en chiffre(entier)!");
			}
			else
			{
				DateTime dateDebut = monthCalendarProjet.SelectionStart;
				DateTime dateFin = monthCalendarProjet.SelectionEnd;
				if (dateDebut != null && dateFin != null)
				{
					cout = Convert.ToInt16(textBoxCoutMO.Text);
					Projet projet = new Projet(textBoxNom.Text, dateDebut, dateFin, cout);
					Donnees.CollectionProjets.Add(projet);
				}
				else
				{
					MessageBox.Show("Veuillez choisir une date de début et une date de fin!");
				}
			}
		}
	}
}
