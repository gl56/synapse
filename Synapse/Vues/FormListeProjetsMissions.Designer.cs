﻿namespace Synapse.Vues
{
	partial class FormListeProjetsMissions
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.dataGridViewProjet = new System.Windows.Forms.DataGridView();
			this.dataGridViewMission = new System.Windows.Forms.DataGridView();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMission)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.groupBox3);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(14, 9);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(502, 336);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Listes";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.dataGridViewProjet);
			this.groupBox2.Location = new System.Drawing.Point(6, 22);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(490, 149);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Projets";
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.dataGridViewMission);
			this.groupBox3.Location = new System.Drawing.Point(6, 177);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(490, 153);
			this.groupBox3.TabIndex = 1;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Missions";
			// 
			// dataGridViewProjet
			// 
			this.dataGridViewProjet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewProjet.Location = new System.Drawing.Point(19, 22);
			this.dataGridViewProjet.Name = "dataGridViewProjet";
			this.dataGridViewProjet.Size = new System.Drawing.Size(447, 109);
			this.dataGridViewProjet.TabIndex = 0;
			this.dataGridViewProjet.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewProjet_RowEnter);
			// 
			// dataGridViewMission
			// 
			this.dataGridViewMission.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridViewMission.Location = new System.Drawing.Point(19, 22);
			this.dataGridViewMission.Name = "dataGridViewMission";
			this.dataGridViewMission.Size = new System.Drawing.Size(447, 114);
			this.dataGridViewMission.TabIndex = 1;
			// 
			// FormListeProjetsMissions
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(528, 348);
			this.Controls.Add(this.groupBox1);
			this.Name = "FormListeProjetsMissions";
			this.Text = "FormListeProjetsMissions";
			this.Load += new System.EventHandler(this.FormListeProjetsMissions_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewProjet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.dataGridViewMission)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.DataGridView dataGridViewMission;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.DataGridView dataGridViewProjet;
	}
}