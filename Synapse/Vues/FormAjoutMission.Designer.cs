﻿namespace Synapse.Vues
{
	partial class FormAjoutMission
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.buttonAjoutMission = new System.Windows.Forms.Button();
			this.comboBoxListeProjet = new System.Windows.Forms.ComboBox();
			this.comboBoxListeIntervenant = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.textBoxNom = new System.Windows.Forms.TextBox();
			this.textBoxDescription = new System.Windows.Forms.TextBox();
			this.textBoxNbHeuresPrevues = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.comboBoxListeProjet);
			this.groupBox1.Controls.Add(this.buttonAjoutMission);
			this.groupBox1.Controls.Add(this.groupBox2);
			this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox1.Location = new System.Drawing.Point(10, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(514, 303);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Mission";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.textBoxNbHeuresPrevues);
			this.groupBox2.Controls.Add(this.textBoxDescription);
			this.groupBox2.Controls.Add(this.textBoxNom);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Controls.Add(this.comboBoxListeIntervenant);
			this.groupBox2.Location = new System.Drawing.Point(21, 22);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(483, 193);
			this.groupBox2.TabIndex = 0;
			this.groupBox2.TabStop = false;
			// 
			// buttonAjoutMission
			// 
			this.buttonAjoutMission.Location = new System.Drawing.Point(361, 221);
			this.buttonAjoutMission.Name = "buttonAjoutMission";
			this.buttonAjoutMission.Size = new System.Drawing.Size(130, 69);
			this.buttonAjoutMission.TabIndex = 1;
			this.buttonAjoutMission.Text = "Ajouter une Mission";
			this.buttonAjoutMission.UseVisualStyleBackColor = true;
			this.buttonAjoutMission.Click += new System.EventHandler(this.buttonAjoutMission_Click);
			// 
			// comboBoxListeProjet
			// 
			this.comboBoxListeProjet.FormattingEnabled = true;
			this.comboBoxListeProjet.Location = new System.Drawing.Point(122, 233);
			this.comboBoxListeProjet.Name = "comboBoxListeProjet";
			this.comboBoxListeProjet.Size = new System.Drawing.Size(218, 24);
			this.comboBoxListeProjet.TabIndex = 2;
			this.comboBoxListeProjet.SelectedIndexChanged += new System.EventHandler(this.comboBoxListeProjet_SelectedIndexChanged);
			// 
			// comboBoxListeIntervenant
			// 
			this.comboBoxListeIntervenant.FormattingEnabled = true;
			this.comboBoxListeIntervenant.Location = new System.Drawing.Point(180, 133);
			this.comboBoxListeIntervenant.Name = "comboBoxListeIntervenant";
			this.comboBoxListeIntervenant.Size = new System.Drawing.Size(290, 24);
			this.comboBoxListeIntervenant.TabIndex = 0;
			this.comboBoxListeIntervenant.SelectedIndexChanged += new System.EventHandler(this.comboBoxListeIntervenant_SelectedIndexChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(17, 28);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(123, 17);
			this.label1.TabIndex = 1;
			this.label1.Text = "Nom de la mission";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(17, 62);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(169, 17);
			this.label2.TabIndex = 2;
			this.label2.Text = "Description  de la mission";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(17, 105);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(213, 17);
			this.label3.TabIndex = 3;
			this.label3.Text = "Temps, en heures, de la mission";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(17, 136);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(157, 17);
			this.label4.TabIndex = 4;
			this.label4.Text = "Executeur de la mission";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(38, 236);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(45, 17);
			this.label5.TabIndex = 3;
			this.label5.Text = "Projet";
			// 
			// textBoxNom
			// 
			this.textBoxNom.Location = new System.Drawing.Point(146, 25);
			this.textBoxNom.Name = "textBoxNom";
			this.textBoxNom.Size = new System.Drawing.Size(324, 23);
			this.textBoxNom.TabIndex = 5;
			// 
			// textBoxDescription
			// 
			this.textBoxDescription.Location = new System.Drawing.Point(192, 62);
			this.textBoxDescription.Name = "textBoxDescription";
			this.textBoxDescription.Size = new System.Drawing.Size(278, 23);
			this.textBoxDescription.TabIndex = 6;
			// 
			// textBoxNbHeuresPrevues
			// 
			this.textBoxNbHeuresPrevues.Location = new System.Drawing.Point(236, 99);
			this.textBoxNbHeuresPrevues.Name = "textBoxNbHeuresPrevues";
			this.textBoxNbHeuresPrevues.Size = new System.Drawing.Size(234, 23);
			this.textBoxNbHeuresPrevues.TabIndex = 7;
			this.textBoxNbHeuresPrevues.Leave += new System.EventHandler(this.textBoxNbHeuresPrevues_Leave);
			// 
			// FormAjoutMission
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(534, 325);
			this.Controls.Add(this.groupBox1);
			this.Name = "FormAjoutMission";
			this.Text = "FormAjoutMission";
			this.Load += new System.EventHandler(this.FormAjoutMission_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.ComboBox comboBoxListeProjet;
		private System.Windows.Forms.Button buttonAjoutMission;
		private System.Windows.Forms.ComboBox comboBoxListeIntervenant;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox textBoxNbHeuresPrevues;
		private System.Windows.Forms.TextBox textBoxDescription;
		private System.Windows.Forms.TextBox textBoxNom;
	}
}