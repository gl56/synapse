﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Utilitaire;
using Synapse.Metier;
using Synapse.Properties;

namespace Synapse.Vues
{
	public partial class FormAjoutMission : Form
	{
		public FormAjoutMission()
		{
			InitializeComponent();
		}
		private int heures;

		private void FormAjoutMission_Load(object sender, EventArgs e)
		{
			comboBoxListeIntervenant.DataSource = Donnees.CollectionIntervenants;
			comboBoxListeProjet.DataSource = Donnees.CollectionProjets;
		}

		private void textBoxNbHeuresPrevues_Leave(object sender, EventArgs e)
		{
			if (textBoxNbHeuresPrevues.Text!= "")
			{
				heures = Convert.ToInt16(textBoxNbHeuresPrevues.Text);
				if (heures == 0)
				{
					MessageBox.Show("Veuillez indiquer le nombre d'heures!");
					textBoxNbHeuresPrevues.Clear();
					textBoxNbHeuresPrevues.Focus();
				}
			}
		}

		private void comboBoxListeIntervenant_SelectedIndexChanged(object sender, EventArgs e)
		{
			Intervenant executant = comboBoxListeIntervenant.SelectedItem as Intervenant;
		}

		private void comboBoxListeProjet_SelectedIndexChanged(object sender, EventArgs e)
		{
			Projet projetSelectionne = comboBoxListeProjet.SelectedItem as Projet;
		}

		private void buttonAjoutMission_Click(object sender, EventArgs e)
		{
			if (textBoxNom.Text == "")
			{
				MessageBox.Show("Veuillez donner un nom à la mission!");
			}
			else if (textBoxDescription.Text == "")
			{
				MessageBox.Show("Que fait la mission?");
			}
			else if (textBoxNbHeuresPrevues.Text == "")
			{
				MessageBox.Show("Combien de temps dure la mission?");
			}
			else if (comboBoxListeIntervenant.SelectedIndex == -1)
			{
				MessageBox.Show("Qui s'occupe la mission?");
			}
			else if (comboBoxListeProjet.SelectedIndex == -1)
			{
				MessageBox.Show("A quelle projet appartient la mission?");
			}
			else
			{
				heures = Convert.ToInt16(textBoxNbHeuresPrevues.Text);
				Projet projetSelectionne = comboBoxListeProjet.SelectedItem as Projet;
				Intervenant executant = comboBoxListeIntervenant.SelectedItem as Intervenant;
				projetSelectionne.AjouterMission(textBoxNom.Text, textBoxDescription.Text, heures, executant);
			}
		}
	}
}
