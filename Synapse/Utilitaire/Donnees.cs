﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metier;

namespace Synapse.Utilitaire
{
	class Donnees
	{
		private static List<Intervenant> _collectionIntervenants;
		private static List<Projet> _collectionProjets;

		public static List<Intervenant> CollectionIntervenants
		{
			get
			{
				if (_collectionIntervenants == null)
				{
					_collectionIntervenants = (List<Intervenant>)Persistances.ChargerDonnees("Intervenant");
					if (_collectionIntervenants == null)
						_collectionIntervenants = new List<Intervenant>();
				}
				return Donnees._collectionIntervenants;
			}
			set { Donnees._collectionIntervenants = value; }
		}

		public static List<Projet> CollectionProjets
		{
			get
			{
				if (_collectionProjets == null)
				{
					_collectionProjets = (List<Projet>)Persistances.ChargerDonnees("Projet");
					if (_collectionProjets == null)
						_collectionProjets = new List<Projet>();
				}
				return Donnees._collectionProjets;
			}
			set { Donnees._collectionProjets = value; }
		}

		public static void SauvegardeDonnees()
		{
			Persistances.SauvegarderDonnees("Intervenant", _collectionIntervenants);
			Persistances.SauvegarderDonnees("Projet", _collectionProjets);
		}
	}
}
